package com.promex.kiosk.service.db;

import com.promex.kiosk.model.DeathUser;
import com.promex.kiosk.repository.DeathUserRepository;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class DeathUserService extends AbstractService<DeathUser, Long> {

    private static final Logger LOGGER = LoggerFactory.getLogger(DeathUserService.class);

    @Autowired
    private DeathUserRepository deathUserRepository;

    @PostConstruct
    public void manage() {
        setRepository(deathUserRepository);
        setLOGGER(LOGGER);
    }

    public void disableUser(String user) {
        try {
            List<DeathUser> deathUserList = deathUserRepository.findAllByUserAndEnable(user, true);
            deathUserList.forEach(p-> {
                p.setEnable(false);
                this.update(p);
            });
        } catch (Exception e) {
            LOGGER.error("Error:[" + ExceptionUtils.getFullStackTrace(e) + "]");
        }
    }

}
