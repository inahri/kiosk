package com.promex.kiosk.repository;

import com.promex.kiosk.model.DeathUser;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface DeathUserRepository extends JpaRepository<DeathUser, Long> {

    List<DeathUser> findAllByUserAndEnable(String user, Boolean enable);

}
