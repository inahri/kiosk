package com.promex.kiosk.model;

import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Entity
@Table(name = "death_vote")
public class DeathUser {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull(message = "Usuario requerido")
    @NotEmpty(message = "Usuario requerido")
    private String user;

    private String name;

    private Integer cosplay;

    private Integer lyric;

    private Date register;

    private Boolean enable;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getCosplay() {
        return cosplay;
    }

    public void setCosplay(Integer cosplay) {
        this.cosplay = cosplay;
    }

    public Integer getLyric() {
        return lyric;
    }

    public void setLyric(Integer lyric) {
        this.lyric = lyric;
    }

    public Date getRegister() {
        return register;
    }

    public void setRegister(Date register) {
        this.register = register;
    }

    public Boolean getEnable() {
        return enable;
    }

    public void setEnable(Boolean enable) {
        this.enable = enable;
    }
}
