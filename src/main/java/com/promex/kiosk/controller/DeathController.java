package com.promex.kiosk.controller;

import com.promex.kiosk.model.DeathUser;
import com.promex.kiosk.service.db.DeathUserService;
import com.promex.kiosk.service.logic.CredentialService;
import org.apache.commons.lang.math.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
@RequestMapping("/death")
public class DeathController {

    @Autowired
    private CredentialService credentialService;

    @Autowired
    private DeathUserService deathUserService;

    @RequestMapping
    public String index(Model model) {
        model.addAttribute("form", new DeathUser());
        return "death/index";
    }

    @PostMapping
    public String index(@Validated @ModelAttribute("form")  DeathUser form, final BindingResult bindingResult, RedirectAttributes redirectAttributes) {
        if  (bindingResult.hasErrors()) {
            return "death/index";
        }
        if  (!NumberUtils.isNumber(form.getUser())) {
            redirectAttributes.addFlashAttribute("SUCCESS_MESSAGE", "Usuario invalido");
            return "redirect:/death";
        }
        if (!credentialService.validateUser(form)) {
            redirectAttributes.addFlashAttribute("SUCCESS_MESSAGE", "Usuario no encontrado");
            return "redirect:/death";
        }
        return "redirect:/death/cosplay/" + form.getUser();
    }

    @GetMapping("/cosplay/{user}")
    public String cosplay(Model model, @PathVariable("user") String user) {
        DeathUser form = new DeathUser();
        form.setUser(user);
        if (!credentialService.validateUser(form)) {
            return "redirect:/death";
        }
        model.addAttribute("form", form);
        return "death/cosplay";
    }

    @PostMapping("/cosplay")
    public String cosplay(Model model, @Validated @ModelAttribute("form")  DeathUser form, final BindingResult bindingResult, RedirectAttributes redirectAttributes) {
        if  (bindingResult.hasErrors()) {
            return "death/cosplay";
        }
        if (!NumberUtils.isNumber(form.getUser())) {
            model.addAttribute("SUCCESS_MESSAGE", "Usuario invalido");
            form.setCosplay(null);
            return "death/cosplay";
        }
        if (form.getCosplay() == null || form.getCosplay() < 1) {
            model.addAttribute("SUCCESS_MESSAGE", "Opcion invalida");
            form.setCosplay(null);
            return "death/cosplay";
        }
        if (!credentialService.validateUser(form)) {
            redirectAttributes.addFlashAttribute("SUCCESS_MESSAGE", "Usuario no encontrado");
            return "redirect:/death";
        }
        return "redirect:/death/lyric/" + form.getUser() + "/" + form.getCosplay();
    }

    @GetMapping("/lyric/{user}/{cosplay}")
    public String lyric(Model model, @PathVariable("user") String user, @PathVariable("cosplay") Integer cosplay) {
        DeathUser form = new DeathUser();
        form.setUser(user);
        form.setCosplay(cosplay);
        if (!credentialService.validateUser(form) || cosplay == null || cosplay < 1) {
            return "redirect:/death";
        }
        model.addAttribute("form", form);
        return "death/lyric";
    }

    @PostMapping("/lyric")
    public String lyric(Model model, @Validated @ModelAttribute("form")  DeathUser form, final BindingResult bindingResult, RedirectAttributes redirectAttributes) {
        if  (bindingResult.hasErrors()) {
            return "death/lyric";
        }
        if (!NumberUtils.isNumber(form.getUser())) {
            model.addAttribute("SUCCESS_MESSAGE", "Usuario invalido");
            return "death/lyric";
        }
        if (form.getCosplay() == null || form.getCosplay() < 1) {
            model.addAttribute("SUCCESS_MESSAGE", "Opcion invalida");
            form.setLyric(null);
            return "redirect:/death";
        }
        if (form.getLyric() == null || form.getLyric() < 1) {
            model.addAttribute("SUCCESS_MESSAGE", "Opcion invalida");
            form.setLyric(null);
            return "death/lyric";
        }
        if (!credentialService.validateUser(form)) {
            model.addAttribute("SUCCESS_MESSAGE", "Usuario no encontrado");
            return "redirect:/lyric";
        }
        deathUserService.disableUser(form.getUser());
        form.setEnable(true);
        DeathUser deathUser = deathUserService.insert(form);
        if (deathUser.getId() != null && deathUser.getId() > 0) {
            redirectAttributes.addFlashAttribute("SUCCESS_MESSAGE", "Voto registrado");
            return "redirect:/death";
        }
        redirectAttributes.addFlashAttribute("SUCCESS_MESSAGE", "Voto incorrecto. Intente más tarde");
        return "redirect:/death";
    }

}
